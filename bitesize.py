#!/usr/bin/python
#
# bitehist.py   Block I/O size histogram.
#               For Linux, uses BCC, eBPF. See .c file.
#
# USAGE: bitesize
#
# Ctrl-C will print the partially gathered histogram then exit.
#
# Copyright (c) 2016 Allan McAleavy
# Licensed under the Apache License, Version 2.0 (the "License")
#
# 05-Feb-2016 Allan McAleavy ran pep8 against file
# 19-Mar-2019 Brendan Gregg  Switched to use tracepoints.

from bcc import BPF
from time import sleep
from init_db import influx_client
from const import DatabaseType
from db_modules import write2db

from datetime import datetime
from time import strftime
bpf_text = """
#include <uapi/linux/ptrace.h>
#include <linux/blkdev.h>

typedef struct dist_key {
    
    u64 slot;
    u32 pid
    char comm[TASK_COMM_LEN];
} dist_key_t;
BPF_PERF_OUTPUT(events);



TRACEPOINT_PROBE(block, block_rq_issue)
{
    struct  dist_key data1={};
    u64 pid_tgid = bpf_get_current_pid_tgid();
    u32 pid = pid_tgid >> 32;
    struct proc_key_t key = {.slot = bpf_log2l(args->bytes / 1024)};
    bpf_probe_read_kernel(&key.name, sizeof(key.name), args->comm);
    dist.increment(key);
    data1.pid=pid;
    data1.comm=bpf_get_current_comm(&comm, sizeof(comm));
    data1.solt=key;
    events.perf_submit(ctx, &data, sizeof(data));
    return 0;
}
"""
# data structure from template
class lmp_data(object):
    def __init__(self,a,b,c,d,e):
            self.time = a
            self.glob = b
            self.pid = c
            self.comm = d
            self.slot = e
            
            
                    
data_struct = {"measurement":'bitesize',
               "time":[],
               "tags":['glob',],
               "fields":['time','pid','comm','slot']}
print("Tracing block device I/O... Hit Ctrl-C to end.")
# load BPF program
b = BPF(text=bpf_text)
print("Tracing block device I/O... Hit Ctrl-C to end.")
print("Tracing block I/O... Hit Ctrl-C to end.")

# process event
def print_event(cpu, data, size):
    event = b["events"].event(data)
         
    test_data = lmp_data(datetime.now().isoformat(),'glob',event.pid,event.comm.decode('utf-8', 'replace'),event.solt)
    print("Tracing block device I/O... Hit Ctrl-C to end.")
    write2db(data_struct, test_data, influx_client, DatabaseType.INFLUXDB.value)
        
        
   # print(("%s Triggered by PID %d (\"%s\"), OOM kill of PID %d (\"%s\")"
   #    ", %d pages, loadavg: %s") % (strftime("%H:%M:%S"), event.fpid,
   #    event.fcomm.decode('utf-8', 'replace'), event.tpid,
   #     event.tcomm.decode('utf-8', 'replace'), event.pages, avgline))

# initialize BPF
b = BPF(text=bpf_text)

b["events"].open_perf_buffer(print_event)
while 1:
    try:
        b.perf_buffer_poll()
    except KeyboardInterrupt:
        exit()